package main

import (
	"fmt"
	"log"
	"time"
)

//0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, …
var (
	wrong int = 0
	ok    int = 0
)

func fib(n int) int {
	if n == 0 {
		return 0
	} else if n == 1 {
		return 1
	} else {
		return fib(n-1) + fib(n-2)
	}
}

func f() {
	input := make(chan int, 1)
	fmt.Println("Start inputing the Fibonacci sequence. Start from 0")
loop:
	for i := 0; ; i++ {
		go getInput(input)
		rightAnswer := fib(i)
		select {
		case x := <-input:
			if x == rightAnswer {
				ok++
				fmt.Println("Right!")
				if ok == 10 {
					fmt.Println("Win")
					break loop
				}
			} else {
				wrong ++
				ok = 0
				fmt.Println("Wrong!")
				fmt.Printf("{\n\t\"fib(i)\":%v,\n\t\"i\":%v\n}\n", rightAnswer, i)
			}
			if wrong == 3 {
				fmt.Println("Lose")
				break loop
			}
			x = 0
			fmt.Println("Input next answer: ")
			continue
		case <-time.After(10 * time.Second):
			wrong ++
			ok = 0
			fmt.Println("Timer expired")
			fmt.Printf("{\n\t\"fib(i)\":%v,\n\t\"i\":%v\n}\n", rightAnswer, i)
			if wrong == 3 {
				fmt.Println("Lose")
				break loop
			}
			fmt.Println("Input next answer: ")
			continue
		}
	}
}
func main() {
	f()
}
func getInput(input chan int) {
	var x int
	_, err := fmt.Scanf("%d", &x)
	if err != nil {
		log.Fatal(err)
	}
	input <- x
}
